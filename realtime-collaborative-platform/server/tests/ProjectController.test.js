const request = require('supertest');
const app = require('../index'); // Adjust the path as necessary

describe('Project API', () => {
    it('GET /api/projects - success', async () => {
        const response = await request(app).get('/api/projects');
        expect(response.statusCode).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
    });

    it('POST /api/projects - success', async () => {
        const response = await request(app)
            .post('/api/projects')
            .send({
                name: 'New Project',
                description: 'A test project.'
            });
        expect(response.statusCode).toBe(201);
        expect(response.body).toHaveProperty('id');
    });
});
