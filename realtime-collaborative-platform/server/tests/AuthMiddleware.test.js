const request = require('supertest');
const app = require('../index'); // Adjust the path as necessary
const jwt = require('jsonwebtoken');

describe('Authentication Middleware', () => {
    it('should verify valid token and allow access', async () => {
        const token = jwt.sign({ user: 'testuser' }, process.env.JWT_SECRET);
        const response = await request(app)
            .get('/api/secure-route')
            .set('Authorization', `Bearer ${token}`);

        expect(response.statusCode).toBe(200);
    });

    it('should deny access when token is invalid', async () => {
        const response = await request(app)
            .get('/api/secure-route')
            .set('Authorization', `Bearer invalidtoken123`);

        expect(response.statusCode).toBe(401);
    });
});
