const ProjectService = require('../services/ProjectService');

exports.createProject = async (req, res) => {
    try {
        const project = await ProjectService.createProject(req.body);
        res.status(201).json(project);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.getProject = async (req, res) => {
    try {
        const { projectId } = req.params;
        const project = await ProjectService.getProjectById(projectId);
        if (!project) {
            return res.status(404).json({ message: 'Project not found' });
        }
        res.json(project);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.updateProject = async (req, res) => {
    try {
        const { projectId } = req.params;
        const updatedProject = await ProjectService.updateProject(projectId, req.body);
        res.json(updatedProject);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.deleteProject = async (req, res) => {
    try {
        const { projectId } = req.params;
        await ProjectService.deleteProject(projectId);
        res.json({ message: 'Project deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};
