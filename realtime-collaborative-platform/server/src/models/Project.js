const { Model, DataTypes } = require('sequelize');
const sequelize = require('../config/db');

class Project extends Model {}

Project.init({
  name: DataTypes.STRING,
  description: DataTypes.TEXT
}, { sequelize, modelName: 'project' });

module.exports = Project;
