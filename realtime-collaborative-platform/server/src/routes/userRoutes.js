const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');

// Register new user
router.post('/register', UserController.registerUser);

// User login
router.post('/login', UserController.loginUser);

// Get user profile
router.get('/profile', UserController.getUserProfile);

module.exports = router;
