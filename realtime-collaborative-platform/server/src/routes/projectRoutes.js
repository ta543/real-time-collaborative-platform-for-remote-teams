const express = require('express');
const router = express.Router();
const ProjectController = require('../controllers/ProjectController');

// Create a new project
router.post('/', ProjectController.createProject);

// Update an existing project
router.put('/:projectId', ProjectController.updateProject);

// Get all projects
router.get('/', ProjectController.getAllProjects);

// Delete a project
router.delete('/:projectId', ProjectController.deleteProject);

module.exports = router;
