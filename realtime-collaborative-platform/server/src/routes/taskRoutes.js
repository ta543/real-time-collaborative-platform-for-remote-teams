const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController');

// Add a new task to a project
router.post('/', TaskController.addTask);

// Update task details
router.put('/:taskId', TaskController.updateTask);

// Get tasks for a specific project
router.get('/project/:projectId', TaskController.getTasksByProject);

// Delete a task
router.delete('/:taskId', TaskController.deleteTask);

module.exports = router;
