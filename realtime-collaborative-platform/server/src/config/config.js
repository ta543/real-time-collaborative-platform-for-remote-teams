require('dotenv').config();

const env = process.env.NODE_ENV || 'development';

const development = {
    app: {
        port: parseInt(process.env.DEV_APP_PORT) || 3000
    },
    db: {
        url: process.env.DEV_DATABASE_URL
    }
};

const production = {
    app: {
        port: parseInt(process.env.PROD_APP_PORT) || 3000
    },
    db: {
        url: process.env.PROD_DATABASE_URL
    }
};

const config = {
    development,
    production
};

module.exports = config[env];
