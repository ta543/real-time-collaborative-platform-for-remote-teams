const { Pool } = require('pg');

const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
    // Uncomment the following line if you are using SSL in production
    // ssl: { rejectUnauthorized: false }
});

module.exports = pool;
