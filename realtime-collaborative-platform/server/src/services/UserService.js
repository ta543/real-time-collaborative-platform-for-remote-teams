const User = require('../models/User');

const createUser = async (userData) => {
    try {
        const user = await User.create(userData);
        return user;
    } catch (error) {
        // Handle errors (e.g., duplicate email)
        throw new Error(error.message);
    }
};

const getUserById = async (id) => {
    try {
        const user = await User.findByPk(id);
        if (!user) {
            throw new Error('User not found');
        }
        return user;
    } catch (error) {
        throw new Error(error.message);
    }
};

module.exports = {
    createUser,
    getUserById
};
