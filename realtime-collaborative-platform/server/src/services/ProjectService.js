const Project = require('../models/Project');

const createProject = async (projectData) => {
    try {
        const project = await Project.create(projectData);
        return project;
    } catch (error) {
        throw new Error(error.message);
    }
};

const getProjectById = async (id) => {
    try {
        const project = await Project.findByPk(id);
        if (!project) {
            throw new Error('Project not found');
        }
        return project;
    } catch (error) {
        throw new Error(error.message);
    }
};

module.exports = {
    createProject,
    getProjectById
};
