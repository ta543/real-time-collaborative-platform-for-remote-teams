const Task = require('../models/Task');

const createTask = async (taskData) => {
    try {
        const task = await Task.create(taskData);
        return task;
    } catch (error) {
        throw new Error(error.message);
    }
};

const getTaskById = async (id) => {
    try {
        const task = await Task.findByPk(id);
        if (!task) {
            throw new Error('Task not found');
        }
        return task;
    } catch (error) {
        throw new Error(error.message);
    }
};

module.exports = {
    createTask,
    getTaskById
};
