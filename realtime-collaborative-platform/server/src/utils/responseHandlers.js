const sendSuccess = (res, data, message = 'Success') => {
    res.status(200).json({
        status: 'success',
        message,
        data
    });
};

const sendError = (res, message = 'Error', statusCode = 400) => {
    res.status(statusCode).json({
        status: 'error',
        message
    });
};

module.exports = {
    sendSuccess,
    sendError
};
