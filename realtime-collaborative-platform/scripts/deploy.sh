#!/bin/bash

# Define paths
BUILD_PATH="realtime-collaborative-platform"
DOCKER_IMAGE="rtc-platform"
REGISTRY_URL="your-registry.com"

# Build the Docker image
echo "Building Docker image..."
docker build -t $DOCKER_IMAGE:latest $BUILD_PATH

# Tag the Docker image
echo "Tagging Docker image..."
docker tag $DOCKER_IMAGE:latest $REGISTRY_URL/$DOCKER_IMAGE:latest

# Push the Docker image to a registry
echo "Pushing Docker image to registry..."
docker push $REGISTRY_URL/$DOCKER_IMAGE:latest

# Apply Kubernetes configurations
echo "Applying Kubernetes configurations..."
kubectl apply -f $BUILD_PATH/k8s/

echo "Deployment completed successfully!"
