#!/bin/bash

# Define paths
CLIENT_PATH="realtime-collaborative-platform/client"
SERVER_PATH="realtime-collaborative-platform/server"

echo "Installing client dependencies..."
cd $CLIENT_PATH && npm install

echo "Installing server dependencies..."
cd $SERVER_PATH && npm install

echo "Setup completed successfully!"
