#!/bin/bash

# Define database credentials
DB_HOST="localhost"
DB_USER="root"
DB_PASS="password"
DB_NAME="rtc_platform_db"

# Path to migration files
MIGRATIONS_PATH="realtime-collaborative-platform/server/migrations"

# Apply migrations
echo "Applying database migrations..."
mysql -h $DB_HOST -u $DB_USER -p$DB_PASS $DB_NAME < $MIGRATIONS_PATH/*.sql

echo "Database migrations completed successfully!"
