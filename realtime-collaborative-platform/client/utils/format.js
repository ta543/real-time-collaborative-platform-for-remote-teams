export const formatDate = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' });
};

export const formatNumber = (number) => {
    return new Intl.NumberFormat('en-US', { style: 'decimal' }).format(number);
};
