import React from 'react';

const Footer: React.FC = () => {
    return (
        <footer>
            <p>© 2024 Real-time Collaborative Platform. All rights reserved.</p>
        </footer>
    );
};

export default Footer;
