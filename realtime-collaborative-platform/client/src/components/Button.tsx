import React from 'react';

interface ButtonProps {
  onClick: () => void;
  label: string;
  styleType: 'primary' | 'secondary';
}

const Button: React.FC<ButtonProps> = ({ onClick, label, styleType }) => {
    return (
        <button className={styleType} onClick={onClick}>
            {label}
        </button>
    );
};

export default Button;
