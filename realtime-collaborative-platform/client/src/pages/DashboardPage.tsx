import React from 'react';

const DashboardPage: React.FC = () => {
    return (
        <div className="dashboard">
            <h1>Dashboard</h1>
            <div className="project-summary">
                <p>Here's a snapshot of your active projects and tasks.</p>
                {/* Additional components like ProjectList or TaskOverview can be added here */}
            </div>
        </div>
    );
};

export default DashboardPage;
