import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Button from '../components/Button';

const HomePage: React.FC = () => {
    const handleLogin = () => {
        console.log('Logging in...');
    };

    return (
        <div>
            <Header title="Welcome to Our Collaboration Platform" />
            <main>
                <p>This is your one-stop platform for efficient team collaboration.</p>
                <Button onClick={handleLogin} label="Log In" styleType="primary" />
            </main>
            <Footer />
        </div>
    );
};

export default HomePage;
