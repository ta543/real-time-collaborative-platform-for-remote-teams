import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';

const AboutPage: React.FC = () => {
    return (
        <div>
            <Header title="About Our Platform" />
            <main>
                <p>Learn more about how our platform can transform your remote team collaboration.</p>
            </main>
            <Footer />
        </div>
    );
};

export default AboutPage;
