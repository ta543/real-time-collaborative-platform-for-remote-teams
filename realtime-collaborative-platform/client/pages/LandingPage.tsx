import React from 'react';

const LandingPage: React.FC = () => {
    return (
        <div className="landing-page">
            <h1>Welcome to the Real-time Collaborative Platform</h1>
            <p>Streamline your team's communication and project management in one place.</p>
            <button>Login</button>
            <button>Sign Up</button>
        </div>
    );
};

export default LandingPage;
